OV-Chipcard strikes back
========================

This repository will house the infrastructure required to send and receive messages from a development environment for the [3rd meetup of DDDNL][1].

[1]: http://www.meetup.com/Domain-Driven-Design-Nederland/events/232023125/
